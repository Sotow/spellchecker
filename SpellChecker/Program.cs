﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SpellChecker
{
    internal class Program
    {
        
        private static void Input(ref string inputStringVocabulary, ref string inputStringText)
        {
            Console.WriteLine("Enter your data:");
            inputStringVocabulary = ReadBlock();
            inputStringText = ReadBlock();
        }
        
        private static string ReadBlock()
        {
            var result = "";
            while (true)                //read line before finding a string of '==='
            {
                var inputLine = Console.ReadLine();
                if (!LineIsEnd(inputLine))
                    result += inputLine + "\n";
                else break;
            }
            return result;
        }
        
        private static bool LineIsEnd(string line)
        {
            var currentValue = true;
            foreach (var ch in line)
            {
                if (ch != '=')
                    currentValue = false;
            }

            return currentValue;
        }

        private static void CloneInput(ref string stringVocabulary, 
                                       string inputStringVocabulary, 
                                       ref string stringText, 
                                       string inputStringText)
        {
            stringVocabulary = inputStringVocabulary;
            stringText = inputStringText;
        }

        private static void RemoveUnwantedSpacesAndEnter(ref string line)
        {
            while (line.Contains("  "))        //remove unnecessary spaces
            {
                line = line.Replace("  ", " ");
            }

            while (line.Contains("\n"))        //remove unnecessary enter
            {
                line = line.Replace("\n", " ");
            }
        }
        
        private static void Replace(ref string outputString, string[] arrayWordsInText, string[] arrayWordsInVocabulary)
        {
            var countWordsInText = arrayWordsInText.Length - 1;
            
            outputString = outputString.Insert(0, "[");
            outputString = outputString.Replace(" ", "] [");
            outputString = outputString.Replace("\n", "]\n[");
            outputString = outputString.Remove(outputString.Length - 1, 1);        //encapsulating every word

            for (var i = 0; i < countWordsInText; i++)
            {
                outputString = outputString.Replace("[" + arrayWordsInText[i] + "]", 
                    WordIsCurrent(arrayWordsInText[i],
                        arrayWordsInVocabulary));
            }                                             //replace each word with one current words or with few words

            outputString = outputString.Replace("[]","");            //capsule removal
        }
        
        private static string WordIsCurrent(string word, string[] wordsInVocabulary)
        {
            int countEditInsert = (word.Length + 1) * 26;
            int countEditRemove = word.Length;
            int countEditAdjacent = word.Length - 1;
            int countEditReplace = word.Length * 26;
            int countEditWord = countEditInsert + countEditRemove + countEditAdjacent + countEditReplace;
                                                //initialize the number of various options for edited words
                        
            string[] arrayEditWords = new string[countEditWord];
            arrayEditWords = ArrayEditWordsInit(arrayEditWords,         
                word, 
                countEditInsert, 
                countEditRemove, 
                countEditAdjacent, 
                countEditReplace);                        //initialize array of edit words
            List<string> editWords = new List<string>();
            editWords.AddRange(arrayEditWords);             //convert array edit words to list
            editWords = editWords.Distinct().ToList();        //remove identical words
            
            List<string> vocabulary = new List<string>();
            vocabulary.AddRange(wordsInVocabulary);        //convert array words in vocabulary to list
            vocabulary = vocabulary.Distinct().ToList();

            if(vocabulary.Contains(word))            //check word in vocabulary
                return word;

            var flag = false;
            var currentWord = "";
            var currentCount = 0;
            foreach (var aWord in editWords)
                if (vocabulary.Contains(aWord))
                {
                    currentWord += aWord + " ";
                    flag = true;
                    currentCount++;
                }
                                    //check word in list edit words
            if (flag)
            {
                if (currentCount > 1)
                {
                    return currentWord.Insert(0, "{").Remove(currentWord.Length) + "}";
                }

                return currentWord.Remove(currentWord.Length - 1);
            }

            return "{" + word + "?}";        //return in other cases
        }
        
        private static string[] ArrayEditWordsInit(string[] arrayEditWords, 
                                           string word, 
                                           int countEditInsert, 
                                           int countEditRemove, 
                                           int countEditAdjacent, 
                                           int countEditReplace)
        {
            for (var i = 0; i < arrayEditWords.Length; i++)        //initialize array edit words
                arrayEditWords[i] = word;

            var k = 0;
            for (var j = 97; j < 123; j++)
                for (var i = 0; i < countEditInsert / 26; i++)
                {
                    arrayEditWords[k] = arrayEditWords[k].Insert(i, Convert.ToString(Convert.ToChar(j)));
                    k++;
                }        //adding variants with single character insertion

            for (var i = countEditInsert; i < countEditInsert + countEditRemove; i++)
                arrayEditWords[i] = arrayEditWords[i].Remove(i - countEditInsert, 1);
                         //adding variants with single character removal
            
            var indexSymbol = 0;
            for (var i = countEditInsert + countEditRemove; 
                 i < countEditInsert + countEditRemove + countEditAdjacent; 
                 i++)
            {
                char firstSymbol = arrayEditWords[i][indexSymbol];
                char secondSymbol = arrayEditWords[i][indexSymbol + 1];
                arrayEditWords[i] = arrayEditWords[i].Remove(indexSymbol, 1)
                                                     .Insert(indexSymbol, secondSymbol.ToString());
                arrayEditWords[i] = arrayEditWords[i].Remove(indexSymbol + 1, 1)
                                                     .Insert(indexSymbol + 1, firstSymbol.ToString());
                indexSymbol++;
            }            //adding variants with adjacent replacement

            k = countEditInsert + countEditRemove + countEditAdjacent;
            for (var j = 97; j < 123; j++)
                for (var i = 0; i < countEditReplace / 26; i++)
                {
                    arrayEditWords[k] = arrayEditWords[k].Remove(i, 1);
                    arrayEditWords[k] = arrayEditWords[k].Insert(i, Convert.ToString(Convert.ToChar(j)));
                    k++;
                }            //adding variants with replace one character with another

            arrayEditWords = arrayEditWords.Distinct().ToArray();        //remove identical words in array edit words

            return arrayEditWords;
        }
        
        private static void Output(string outputString)
        {
            Console.WriteLine("\n" + "Edit text:");
            Console.WriteLine(outputString);
        }
        
        
        private static void Main(string[] args)
        {
            string inputStringVocabulary = "";
            string inputStringText = "";
            Input(ref inputStringVocabulary, ref inputStringText);        //reading dictionary blocks and text
    
            var stringVocabulary = "";
            var stringText = "";
            CloneInput(ref stringVocabulary, inputStringVocabulary, ref stringText, inputStringText);        //creating copies of the vocabulary and text
            
            RemoveUnwantedSpacesAndEnter(ref stringVocabulary);        //remove unnecessary characters in copy vocabulary
            RemoveUnwantedSpacesAndEnter(ref stringText);        //remove unnecessary characters in copy text
            string[] arrayWordsInVocabulary = stringVocabulary.Split(new char[] {' '});        //creating an array of words in copy vocabulary
            string[] arrayWordsInText = stringText.Split(new char[] {' '});        //creating an array of words in copy text

            var outputString = inputStringText;
            Replace(ref outputString, arrayWordsInText, arrayWordsInVocabulary);        //text editing 
            
            Output(outputString);        //write text editing
            
        }
    }
}
